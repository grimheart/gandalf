package gandalf

import (
	"reflect"
	"testing"

	"github.com/santhosh-tekuri/jsonschema"
)

func TestRequiredKeysFromSchema(t *testing.T) {
	t.Run("Test requiredKeysFromSchema with a simple nested schema", func(t *testing.T) {

		c := jsonschema.NewCompiler()
		schema, err := c.Compile("some.json")
		if err != nil {
			t.Error(err)
		}
		got := requiredKeysFromSchema(schema)
		want := map[string]interface{}{
			"dokumentation": map[string]interface{}{
				"betriebskonzept": "",
				"wap":             "",
			},
			"name": "",
		}
		deepEqualCheck(t, got, want)
	})
}
func TestUnwrap(t *testing.T) {
	t.Run("Unwrap a map of nested requiredKeys into a string with each key on its level each on a new line", func(t *testing.T) {
		in := map[string]interface{}{
			"dokumentation": map[string]interface{}{
				"betriebskonzept": "",
				"wap":             "",
			},
			"name": "",
		}
		got := unwrap(in, []string{}, 0)
		want := `dokumentation: 
  betriebskonzept: 
  wap: 
name: 
`
		deepEqualCheck(t, got, want)
	})
	t.Run("Unwrap a map of nested requiredKeys which names repeat under different levels into a string with each key on its level each on a new line", func(t *testing.T) {
		in := map[string]interface{}{
			"name": "",
			"dokumentation": map[string]interface{}{
				"betriebskonzept": "",
				"wap": map[string]interface{}{
					"name": "",
				},
			},
		}
		got := unwrap(in, []string{}, 0)
		want := `dokumentation: 
  betriebskonzept: 
  wap: 
    name: 
name: 
`
		deepEqualCheck(t, got, want)
	})
}

func deepEqualCheck(t *testing.T, got, want interface{}) {
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Got \n%s\n, wanted \n%s\n", got, want)

	}
}
