package gandalf

import (
	"fmt"
	"reflect"
	"sort"
	"strings"

	"github.com/santhosh-tekuri/jsonschema"
)

func requiredKeysFromSchema(s *jsonschema.Schema) map[string]interface{} {
	result := make(map[string]interface{})
	for _, req := range s.Required {
		props, ok := s.Properties[req]
		if ok {
			result[req] = requiredKeysFromSchema(props)
		} else {
			result[req] = ""
		}
	}
	return result
}
func unwrap(in map[string]interface{}, keys []string, level int) string {
	var result string
	sortedKeys := sortKeys(in)
	for _, key := range sortedKeys {
		result += buildResultString(in, key, keys, level)
	}
	return result
}

func stringInSlice(str string, slice []string) bool {
	for _, s := range slice {
		if str == s {
			return true
		}
	}
	return false
}
func sortKeys(in map[string]interface{}) []string {
	var sortedKeys []string
	for key := range in {
		sortedKeys = append(sortedKeys, key)
	}
	sort.Strings(sortedKeys)
	return sortedKeys
}

func formatString(key string, level int) string {
	return fmt.Sprintf("%s%s: \n", strings.Repeat("  ", level), key)
}
func buildResultString(in map[string]interface{}, key string, keys []string, level int) string {
	var result string
	val := in[key]
	typeOf := reflect.TypeOf(val).Kind()
	switch typeOf {
	case reflect.Map:
		result += formatString(key, level)
		keys = append(keys, key)
		result += unwrap(val.(map[string]interface{}), keys, level+1)
	case reflect.String:
		result += buildResultStringWithRedundancyCheck(key, result, keys, level)
	}
	return result
}

func buildResultStringWithRedundancyCheck(key, result string, keys []string, level int) string {
	if !stringInSlice(key, keys) {
		result += formatString(key, level)
		keys = append(keys, key)
	}
	return result
}
